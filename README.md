# disease-trends

hci disease trends

1. Ensure that the following files exists in the data folder.

- disease_type.csv
- rainfall-monthly-total.csv
- raw_cases.csv
- temp-monthly-total.csv

2. Ensure that the following library is installed:

python3
python library "isoweek"

```pip install isoweek```

3. Run the following command in the directory to start server running: 


```python start_server.py```

4. Once server has started, go to your browser and access website via:

```localhost:8000```
